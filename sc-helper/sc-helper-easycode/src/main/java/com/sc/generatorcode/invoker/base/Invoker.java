package com.sc.generatorcode.invoker.base;

/**
 * @author ：wust
 * Date   2018-09-10
 */
public interface Invoker {

    public void execute();

}
