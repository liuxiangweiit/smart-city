package com.sc.generatorcode.task;

import com.sc.generatorcode.entity.ColumnInfo;
import com.sc.generatorcode.task.base.AbstractTask;
import com.sc.generatorcode.utils.ConfigUtil;
import com.sc.generatorcode.utils.FileUtil;
import com.sc.generatorcode.utils.FreemarkerConfigUtils;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author ：wust
 * @date：2020-06-30
 */
public class ExportExcelXmlTask extends AbstractTask {
    public ExportExcelXmlTask(String className, String tableName,List<ColumnInfo> infos) {
        super(className,tableName,infos);
    }

    @Override
    public void run() throws IOException, TemplateException {
        String name = super.getName()[1];

        String xmlDir = FileUtil.getResourcePath().concat("/easyexcel/export/xml");

        StringBuffer sql = new StringBuffer("SELECT ");
        StringBuffer fields = new StringBuffer();
        if(columnInfos != null && columnInfos.size() > 0){
            for (ColumnInfo columnInfo : columnInfos) {
                if("id".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("createrId".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("createrName".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("createTime".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("modifyId".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("modifyName".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("modifyTime".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("projectId".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("companyId".equals(columnInfo.getPropertyName())){
                    continue;
                }

                sql.append(columnInfo.getColumnName()).append(",");

                fields.append("<field column=\""+columnInfo.getColumnName()+"\" label=\""+columnInfo.getColumnLabel()+"\"></field>\n");
            }
            sql = new StringBuffer(sql.substring(0,sql.lastIndexOf(",")));
        }
        sql.append(" FROM ").append(tableName);
        sql.append("\n WHERE 1=1 \n");
        sql.append("<#if projectId??>\n");
        sql.append("\tAND project_id = '${projectId}'\n");
        sql.append("</#if>\n");
        sql.append("<#if companyId??>\n");
        sql.append("\tAND company_id = '${companyId}'\n");
        sql.append("</#if>");

        Map<String, String> data = new HashMap<>();
        String xmlName = ConfigUtil.getConfiguration().getServerName().concat("-").concat(name).replaceAll("-","_");
        data.put("xmlName", xmlName);
        data.put("sheetLabel", "导出列表");
        data.put("sqlId", UUID.randomUUID().toString());
        data.put("sql",sql.toString());
        data.put("fields", fields.toString());

        String fileName = xmlName.concat(".xml");
        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_EXPORT_EXCEL_XML, data, xmlDir,fileName);
    }
}
