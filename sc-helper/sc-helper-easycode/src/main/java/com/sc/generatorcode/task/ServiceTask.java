package com.sc.generatorcode.task;

import com.sc.generatorcode.task.base.AbstractTask;
import com.sc.generatorcode.utils.ConfigUtil;
import com.sc.generatorcode.utils.FileUtil;
import com.sc.generatorcode.utils.FreemarkerConfigUtils;
import com.sc.generatorcode.utils.StringUtil;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ：wust
 * Date   2019/1/24
 */
public class ServiceTask extends AbstractTask {

    public ServiceTask(String className) {
        super(className);
    }

    @Override
    public void run() throws IOException, TemplateException {
        String name = super.getName()[1];
        String entityPackageName = ConfigUtil.getConfiguration().getBasePackage().getEntityPackageName() + "." + name;

        Map<String, String> interfaceData = new HashMap<>();
        interfaceData.put("BasePackageName", ConfigUtil.getConfiguration().getBasePackage().getBase());
        interfaceData.put("InterfacePackageName", ConfigUtil.getConfiguration().getBasePackage().getInterf());
        interfaceData.put("EntityPackageName", entityPackageName);
        interfaceData.put("Author", ConfigUtil.getConfiguration().getAuthor());
        interfaceData.put("DateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        interfaceData.put("ClassName", className);

        String filePath = FileUtil.getSourcePath() + StringUtil.package2Path(ConfigUtil.getConfiguration().getBasePackage().getInterf());
        String fileName = className + "Service.java";
        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_INTERFACE, interfaceData, filePath,fileName);
    }
}
