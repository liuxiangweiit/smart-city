package com.sc.common.interceptors.context;

import com.alibaba.fastjson.JSONObject;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.enums.ApplicationEnum;
import com.sc.common.util.MyStringUtils;
import com.sc.common.util.WebUtil;

/**
 * @author ：wust
 * @date ：Created in 2019/8/8 10:48
 * @description：内部feign请求走这里设置上下文
 * @version:
 */
public class FeignStrategy implements IStrategy{

    @Override
    public void setDefaultBusinessContext() {
        String ctxStr = MyStringUtils.null2String(WebUtil.getRequest().getHeader(ApplicationEnum.X_CTX.getStringValue()));

        DefaultBusinessContext ctx = JSONObject.parseObject(ctxStr,DefaultBusinessContext.class);

        DefaultBusinessContext.getContext().setAccountId(ctx.getAccountId());

        DefaultBusinessContext.getContext().setAccountCode(ctx.getAccountCode());

        DefaultBusinessContext.getContext().setAccountName(ctx.getAccountName());

        DefaultBusinessContext.getContext().setAccountType(ctx.getAccountType());

        DefaultBusinessContext.getContext().setProjectId(ctx.getProjectId());

        DefaultBusinessContext.getContext().setBranchCompanyId(ctx.getBranchCompanyId());

        DefaultBusinessContext.getContext().setParentCompanyId(ctx.getParentCompanyId());

        DefaultBusinessContext.getContext().setAgentId(ctx.getAgentId());

        DefaultBusinessContext.getContext().setDataSourceId(ctx.getDataSourceId());

        DefaultBusinessContext.getContext().setUser(ctx.getUser());

        DefaultBusinessContext.getContext().setCustomer(ctx.getCustomer());

        DefaultBusinessContext.getContext().setxAuthToken(ctx.getxAuthToken());

        DefaultBusinessContext.getContext().setLocale(ctx.getLocale());
    }
}
