/**
 * Created by wust on 2019-12-16 16:46:43
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.common.util;

import tk.mybatis.mapper.genid.GenId;

/**
 * @author: wust
 * @date: Created in 2019-12-16 16:46:43
 * @description:
 *
 */
public class MyGenId implements GenId<Long> {
    @Override
    public Long genId(String table, String column) {
        return MyIdUtil.getId();
    }
}
