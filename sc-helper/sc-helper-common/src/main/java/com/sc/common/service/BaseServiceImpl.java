package com.sc.common.service;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.dto.WebResponseDto;
import org.apache.ibatis.session.RowBounds;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import java.util.List;

/**
 * @author ：wust
 * @date ：Created in 2019/7/30 14:10
 * @description：服务层默认实现，满足大部分需求，个性化需求在自己的服务层实现
 * @version:
 */
public abstract class BaseServiceImpl<T> implements BaseService<T>{
    protected abstract IBaseMapper getBaseMapper();

    @Override
    public List<?> selectByPageNumSize(Object search, int pageNum, int pageSize) {
        return getBaseMapper().selectByPageNumSize(search,pageNum,pageSize);
    }

    @Override
    public T selectByPrimaryKey(Object key) {
        T result = (T)getBaseMapper().selectByPrimaryKey(key);
        return result;
    }

    @Override
    public boolean existsWithPrimaryKey(Object key) {
        boolean result = getBaseMapper().existsWithPrimaryKey(key);
        return result;
    }


    @Override
    public T selectOne(T search) {
        T result = (T)getBaseMapper().selectOne(search);
        return result;
    }

    @Override
    public T selectOneByExample(Example example) {
        T result = (T)getBaseMapper().selectOneByExample(example);
        return result;
    }

    @Override
    public List<T> select(T search){
        return getBaseMapper().select(search);
    }

    @Override
    public List<T> selectByExample(Example example) {
        return getBaseMapper().selectByExample(example);
    }

    @Override
    public List<T> selectAll() {
        return getBaseMapper().selectAll();
    }

    @Override
    public int selectCount(T search) {
        return  getBaseMapper().selectCount(search);
    }

    @Override
    public int selectCountByExample(Example example) {
        return  getBaseMapper().selectCountByExample(example);
    }

    @Override
    public List<T> selectByRowBounds(T var1, RowBounds var2) {
        return getBaseMapper().selectByRowBounds(var1,var2);
    }

    @Override
    public List<T> selectByExampleAndRowBounds(T var1, RowBounds var2) {
        return getBaseMapper().selectByExampleAndRowBounds(var1,var2);
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public int insert(T entity){
        return getBaseMapper().insert(entity);
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public int insertSelective(T entity){
        return getBaseMapper().insertSelective(entity);
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public int insertList(List entities){
        return getBaseMapper().insertList(entities);
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public int updateByPrimaryKey(T entity) {
        int result = getBaseMapper().updateByPrimaryKey(entity);
        return result;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public int updateByPrimaryKeySelective(T entity) {
        int result = getBaseMapper().updateByPrimaryKeySelective(entity);
        return result;
    }

    @Override
    public int updateByExample(T var1,Example example) {
        int result = getBaseMapper().updateByExample(var1,example);
        return result;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public int updateByExampleSelective(T var1, Example example) {
        int result = getBaseMapper().updateByExampleSelective(var1,example);
        return result;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public int deleteByPrimaryKey(Long key){
        return getBaseMapper().deleteByPrimaryKey(key);
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public int deleteByExample(Object example){
        return getBaseMapper().deleteByExample(example);
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public int batchUpdate(List entities){
        int result = getBaseMapper().batchUpdate(entities);
        return result;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public int batchDelete(List keys){
        return getBaseMapper().batchDelete(keys);
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
