package com.sc.common.aspect;


import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.crypto.digest.MD5;
import com.alibaba.fastjson.JSONObject;
import com.sc.common.enums.EhcacheKeyEnum;
import com.sc.common.enums.RabbitMqOperationTypeEnum;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.exception.BusinessException;
import com.sc.common.util.MqSenderUtil;
import com.sc.common.util.MyStringUtils;
import com.sc.common.util.SpringContextHolder;
import com.sc.common.util.cache.EhcacheTools;
import com.sc.common.util.cache.SpringRedisTools;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;
import tk.mybatis.mapper.entity.Example;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 缓存拦截器，先查缓存再查数据库
 * 对于简单的主键=数据库实体形式直接在本拦截器实时更新。对于其他复杂的缓存，使用MqSenderUtil.cacheSender通知各个缓存业务对象去更新。
 */
public class BaseCacheAspect extends BaseAspect{

    public static int DATA_EXP = 30*24*60*60;

    private static final String EHCACHE_KEY_POCACHE="poCache";
    private static final String EHCACHE_KEY_POCACHEIFABSENT="poCacheIfAbsent";


    public Object around(ProceedingJoinPoint jp, int annotationType) throws Throwable {
        Object[] args = jp.getArgs();
        Signature sig = jp.getSignature();
        if (sig instanceof MethodSignature) {
            MethodSignature msig = (MethodSignature) sig;
            Method currentMethod = jp.getTarget().getClass().getMethod(msig.getName(), msig.getParameterTypes());
            if (null != currentMethod) {
                Object resultObj = null;
                EhcacheTools ehcacheTools = SpringContextHolder.getBean("ehcacheTools");
                SpringRedisTools springRedisTools = SpringContextHolder.getBean("springRedisTools");
                String methodName = currentMethod.getName();

                Class proxyClass = getProxyClass(jp);
                Class returnClass = getGenericClass(proxyClass);
                String source = returnClass.getSimpleName();

                if("selectByPrimaryKey".equals(methodName)){
                    Object primaryKey = null;
                    Object obj = args[0];
                    if(obj instanceof Long){
                        primaryKey = obj;
                    }else if(obj instanceof Map){
                        Map objMap = (Map)obj;
                        if(objMap != null && objMap.containsKey("id")){
                            primaryKey = objMap.get("id");
                        }
                    }

                    if(!isNull(primaryKey)){
                        if(annotationType == 1){
                            String localCachedKey = String.format(EhcacheKeyEnum.EHCACHE_KEY_SELECT_BY_PRIMARY_KEY.getStringValue(),source, Convert.toStr(primaryKey));
                            Object localCachedObj = ehcacheTools.get(EHCACHE_KEY_POCACHE,localCachedKey,returnClass);
                            if(localCachedObj != null){ // 本地缓存有则直接返回
                                return localCachedObj;
                            }else{
                                localCachedObj = ehcacheTools.get(EHCACHE_KEY_POCACHEIFABSENT,localCachedKey,returnClass);
                                if(localCachedObj != null){ // 防止击穿
                                    return localCachedObj;
                                }

                                resultObj = jp.proceed();
                                if(resultObj != null){
                                    ehcacheTools.put(EHCACHE_KEY_POCACHE,localCachedKey,resultObj);
                                }else{
                                    ehcacheTools.put(EHCACHE_KEY_POCACHEIFABSENT,localCachedKey,returnClass.newInstance());
                                }
                            }
                        }else if(annotationType == 2){
                            String redisKey = String.format(RedisKeyEnum.REDIS_KEY_PRIMARY_SELECT_BY_PRIMARY_KEY.getStringValue(), source,Convert.toStr(primaryKey));
                            if(springRedisTools.hasKey(redisKey)){
                                Object redisObj = springRedisTools.getByKey(redisKey);
                                if(redisObj != null){ // 分布式缓存有则直接返回
                                    redisObj = JSONObject.parseObject(redisObj.toString(),returnClass);
                                    return redisObj;
                                }else{
                                    return null;
                                }
                            }else{
                                resultObj = jp.proceed();
                                if(resultObj != null){
                                    springRedisTools.addData(redisKey,JSONObject.toJSONString(resultObj),DATA_EXP,TimeUnit.SECONDS);
                                }else{
                                    springRedisTools.addData(redisKey,returnClass.newInstance(),1, TimeUnit.MINUTES);
                                }
                            }
                        }else{
                            /**
                             * 1.先查本地缓存
                             */
                            String localCachedKey = String.format(EhcacheKeyEnum.EHCACHE_KEY_SELECT_BY_PRIMARY_KEY.getStringValue(), source,Convert.toStr(primaryKey));
                            Object localCachedObj = ehcacheTools.get(EHCACHE_KEY_POCACHE,localCachedKey,returnClass);
                            if(localCachedObj != null){ // 本地缓存有则直接返回
                                return localCachedObj;
                            }else{
                                localCachedObj = ehcacheTools.get(EHCACHE_KEY_POCACHEIFABSENT,localCachedKey,returnClass);
                                if(localCachedObj != null){ // 防止击穿
                                    return localCachedObj;
                                }

                                resultObj = jp.proceed();
                                if(resultObj != null){
                                    ehcacheTools.put(EHCACHE_KEY_POCACHE,localCachedKey,resultObj);
                                }else{
                                    ehcacheTools.put(EHCACHE_KEY_POCACHEIFABSENT,localCachedKey,returnClass.newInstance());
                                }
                            }

                            /**
                             * 2.本地缓存没有再查分布式缓存
                             */
                            String redisKey = String.format(RedisKeyEnum.REDIS_KEY_PRIMARY_SELECT_BY_PRIMARY_KEY.getStringValue(), source,Convert.toStr(primaryKey));
                            if(springRedisTools.hasKey(redisKey)){
                                Object redisObj = springRedisTools.getByKey(redisKey);
                                if(redisObj != null){ // 分布式缓存有则直接返回
                                    redisObj = JSONObject.parseObject(redisObj.toString(),returnClass);
                                    return redisObj;
                                }else{
                                    return null;
                                }
                            }

                            /**
                             * 3.缓存都没有最后查数据库
                             */
                            resultObj = jp.proceed();
                            if(resultObj != null){
                                ehcacheTools.put(EHCACHE_KEY_POCACHE,localCachedKey,resultObj);
                                springRedisTools.addData(redisKey,JSONObject.toJSONString(resultObj),DATA_EXP,TimeUnit.SECONDS);
                            }else{
                                ehcacheTools.put(EHCACHE_KEY_POCACHEIFABSENT,localCachedKey,returnClass.newInstance());
                                springRedisTools.addData(redisKey,returnClass.newInstance(),1, TimeUnit.MINUTES);
                            }
                        }
                    }else {
                        throw new BusinessException("缺少primaryKey参数");
                    }
                    return resultObj;
                }else if("existsWithPrimaryKey".equals(methodName)){
                    Object primaryKey = null;
                    Object obj = args[0];
                    if(obj instanceof Long){
                        primaryKey = obj;
                    }else if(obj instanceof Map){
                        Map objMap = (Map)obj;
                        if(objMap != null && objMap.containsKey("id")){
                            primaryKey = objMap.get("id");
                        }
                    }

                    if(!isNull(primaryKey)){
                        if(annotationType == 1){
                            String localCachedKey = String.format(EhcacheKeyEnum.EHCACHE_KEY_SELECT_BY_PRIMARY_KEY.getStringValue(),source, Convert.toStr(primaryKey));
                            Object localCachedObj = ehcacheTools.get(EHCACHE_KEY_POCACHE,localCachedKey);
                            if(localCachedObj != null){
                                return localCachedObj;
                            }

                            localCachedObj = ehcacheTools.get(EHCACHE_KEY_POCACHEIFABSENT,localCachedKey);
                            if(localCachedObj != null){ // 防止击穿
                                return localCachedObj;
                            }

                            resultObj = jp.proceed();
                            if(resultObj != null){
                                ehcacheTools.put(EHCACHE_KEY_POCACHE,localCachedKey,resultObj);
                                return resultObj;
                            }else{
                                ehcacheTools.put(EHCACHE_KEY_POCACHEIFABSENT,localCachedKey,false);
                                return false;
                            }
                        }else if(annotationType == 2){
                            String redisKey = String.format(RedisKeyEnum.REDIS_KEY_PRIMARY_SELECT_BY_PRIMARY_KEY.getStringValue(),source, Convert.toStr(primaryKey));
                            if(springRedisTools.hasKey(redisKey)){
                                Object redisObj = springRedisTools.getByKey(redisKey);
                                if(redisObj != null){ // 分布式缓存有则直接返回
                                    return redisObj;
                                }else{
                                    return false;
                                }
                            }

                            resultObj = jp.proceed();
                            if(resultObj != null){
                                springRedisTools.addData(redisKey,resultObj,DATA_EXP,TimeUnit.SECONDS);
                            }else{
                                springRedisTools.addData(redisKey,false,1, TimeUnit.MINUTES);
                                return false;
                            }
                        }else{
                            /**
                             * 1.先查本地缓存
                             */
                            String localCachedKey = String.format(EhcacheKeyEnum.EHCACHE_KEY_SELECT_BY_PRIMARY_KEY.getStringValue(),source, Convert.toStr(primaryKey));
                            Object localCachedObj = ehcacheTools.get(EHCACHE_KEY_POCACHE,localCachedKey);
                            if(localCachedObj != null){
                                return localCachedObj;
                            }

                            localCachedObj = ehcacheTools.get(EHCACHE_KEY_POCACHEIFABSENT,localCachedKey);
                            if(localCachedObj != null){ // 防止击穿
                                return localCachedObj;
                            }



                            /**
                             * 2.本地缓存没有再查分布式缓存
                             */
                            String redisKey = String.format(RedisKeyEnum.REDIS_KEY_PRIMARY_SELECT_BY_PRIMARY_KEY.getStringValue(),source, Convert.toStr(primaryKey));
                            if(springRedisTools.hasKey(redisKey)){
                                Object redisObj = springRedisTools.getByKey(redisKey);
                                if(redisObj != null){ // 分布式缓存有则直接返回
                                    return redisObj;
                                }else{
                                    return false;
                                }
                            }

                            /**
                             * 3.缓存都没有最后查数据库
                             */
                            resultObj = jp.proceed();
                            if(resultObj != null){
                                ehcacheTools.put(EHCACHE_KEY_POCACHE,localCachedKey,resultObj);
                                springRedisTools.addData(redisKey,resultObj,DATA_EXP,TimeUnit.SECONDS);
                            }else{
                                ehcacheTools.put(EHCACHE_KEY_POCACHEIFABSENT,localCachedKey,false);
                                springRedisTools.addData(redisKey,false,1, TimeUnit.MINUTES);
                                return false;
                            }
                        }
                    }else {
                        throw new BusinessException("缺少primaryKey参数");
                    }
                    return resultObj;
                }else if("selectOne".equals(methodName)){
                    Object search = args[0];
                    Object[] values = ReflectUtil.getFieldsValue(search);
                    if(values != null && values.length > 0){
                        StringBuffer valueStringBuffer = new StringBuffer();
                        for (Object value : values) {
                            if(MyStringUtils.isBlank(MyStringUtils.null2String(Convert.toStr(value)))){
                                continue;
                            }
                            valueStringBuffer.append(value);
                        }

                        String key = MD5.create().digestHex16(valueStringBuffer.toString());
                        String redisKey = String.format(RedisKeyEnum.REDIS_KEY_SELECT_ONE.getStringValue(),source, Convert.toStr(key));
                        String localCachedKey = String.format(EhcacheKeyEnum.EHCACHE_KEY_SELECT_ONE.getStringValue(),source, Convert.toStr(key));
                        Object primaryKey = ehcacheTools.get(EHCACHE_KEY_POCACHE,localCachedKey);
                        if(!isNull(primaryKey)){
                            String className =  proxyClass.getName().substring(proxyClass.getName().lastIndexOf(".")+1);
                            String beanName = className.substring(0,1).toLowerCase() + className.substring(1);
                            Object bean = SpringContextHolder.getBean(beanName);
                            Method mtd = proxyClass.getMethod("selectByPrimaryKey", Object.class);
                            Object obj2 = mtd.invoke(bean,primaryKey);
                            if(obj2 != null){
                                return obj2;
                            }
                        }

                        resultObj = jp.proceed();
                        if(resultObj != null){
                            Object id = ReflectUtil.getFieldValue(resultObj,"id");
                            if(annotationType == 1){
                                ehcacheTools.put(EHCACHE_KEY_POCACHE,localCachedKey,id);
                            }else if(annotationType == 2){
                                springRedisTools.addData(redisKey,id,DATA_EXP,TimeUnit.SECONDS);
                            }else{
                                ehcacheTools.put(EHCACHE_KEY_POCACHE,localCachedKey,id);
                                springRedisTools.addData(redisKey,id,DATA_EXP,TimeUnit.SECONDS);
                            }
                        }else{
                            if(annotationType == 1){
                                ehcacheTools.put(EHCACHE_KEY_POCACHE,localCachedKey,0);
                            }else if(annotationType == 2){
                                springRedisTools.addData(redisKey,null,1, TimeUnit.MINUTES);
                            }else{
                                ehcacheTools.put(EHCACHE_KEY_POCACHE,localCachedKey,0);
                                springRedisTools.addData(redisKey,null,1, TimeUnit.MINUTES);
                            }
                        }
                    }
                    return resultObj;
                }else if("selectOneByExample".equals(methodName)){
                    Object exampleObj = args[0];
                    Example example = (Example)exampleObj;
                    List<Example.Criteria> list = example.getOredCriteria();
                    if(CollectionUtil.isNotEmpty(list)){
                        for (Example.Criteria criteria : list) {
                            List<Example.Criterion> criterions = criteria.getCriteria();
                            if(CollectionUtil.isNotEmpty(criterions)){
                                StringBuffer valueStringBuffer = new StringBuffer();
                                for (Example.Criterion criterion : criterions) {
                                    String s = criterion.getCondition() + criterion.getAndOr() + criterion.getValue();
                                    valueStringBuffer.append(s);
                                }

                                String key = MD5.create().digestHex16(valueStringBuffer.toString());
                                String redisKey = String.format(RedisKeyEnum.REDIS_KEY_SELECT_ONE_EXAMPLE.getStringValue(), source,Convert.toStr(key));
                                String localCachedKey = String.format(EhcacheKeyEnum.EHCACHE_KEY_SELECT_ONE_EXAMPLE.getStringValue(), source,Convert.toStr(key));

                                Object primaryKey = ehcacheTools.get(EHCACHE_KEY_POCACHE,localCachedKey);
                                if(!isNull(primaryKey)){
                                    String className =  proxyClass.getName().substring(proxyClass.getName().lastIndexOf(".")+1);
                                    String beanName = className.substring(0,1).toLowerCase() + className.substring(1);
                                    Object bean = SpringContextHolder.getBean(beanName);
                                    Method mtd = proxyClass.getMethod("selectByPrimaryKey", Object.class);
                                    Object obj2 = mtd.invoke(bean,primaryKey);
                                    if(obj2 != null){
                                        return obj2;
                                    }
                                }else{
                                    primaryKey = ehcacheTools.get(EHCACHE_KEY_POCACHEIFABSENT,localCachedKey);
                                    if(!isNull(primaryKey)){
                                        resultObj = jp.proceed();
                                        if(resultObj != null){
                                            Object id = ReflectUtil.getFieldValue(resultObj,"id");
                                            if(annotationType == 1){
                                                ehcacheTools.put(EHCACHE_KEY_POCACHE,localCachedKey,id);
                                            }else if(annotationType == 2){
                                                springRedisTools.addData(redisKey,id,DATA_EXP,TimeUnit.SECONDS);
                                            }else{
                                                ehcacheTools.put(EHCACHE_KEY_POCACHE,localCachedKey,id);
                                                springRedisTools.addData(redisKey,id,DATA_EXP,TimeUnit.SECONDS);
                                            }
                                        }else{
                                            if(annotationType == 1){
                                                ehcacheTools.put(EHCACHE_KEY_POCACHEIFABSENT,localCachedKey,0);
                                            }else if(annotationType == 2){
                                                springRedisTools.addData(redisKey,null,1, TimeUnit.MINUTES);
                                            }else{
                                                ehcacheTools.put(EHCACHE_KEY_POCACHEIFABSENT,localCachedKey,0);
                                                springRedisTools.addData(redisKey,null,1, TimeUnit.MINUTES);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return resultObj;
                }
            }
        }
        return jp.proceed();
    }


    public void afterReturning(JoinPoint jp, int annotationType) throws Throwable {
        Object[] args = jp.getArgs();
        Signature sig = jp.getSignature();
        if (sig instanceof MethodSignature) {
            MethodSignature msig = (MethodSignature) sig;
            Method currentMethod = jp.getTarget().getClass().getMethod(msig.getName(), msig.getParameterTypes());
            if (null != currentMethod) {
                EhcacheTools ehcacheTools = SpringContextHolder.getBean("ehcacheTools");
                SpringRedisTools springRedisTools = SpringContextHolder.getBean("springRedisTools");
                String methodName = currentMethod.getName();

                Class proxyClass = getProxyClass(jp);
                Class returnClass = getGenericClass(proxyClass);
                String source = returnClass.getSimpleName();

                if("insert".equals(methodName)){
                    Object parameterObj = args[0];
                    if(parameterObj != null){
                        Object primaryKey = ReflectUtil.getFieldValue(parameterObj,"id");
                        cache(annotationType,primaryKey,parameterObj,source, RabbitMqOperationTypeEnum.OPR_ADD.name());
                    }else{
                        throw new BusinessException("缺少实体参数");
                    }
                }else if("insertList".equals(methodName)){
                    Object parameterObj = args[0];
                    if(parameterObj != null){
                        List list = (List)parameterObj;
                        for (Object o : list) {
                            Object primaryKey = ReflectUtil.getFieldValue(o,"id");
                            if(isNull(primaryKey)){
                                continue;
                            }
                            cache(annotationType,primaryKey,o,source, RabbitMqOperationTypeEnum.OPR_ADD.name());
                        }
                    }else{
                        throw new BusinessException("缺少实体集合参数");
                    }
                }else if("updateByPrimaryKey".equals(methodName)){
                    Object primaryKey = null;
                    Object obj = args[0];
                    if(obj instanceof Long){
                        primaryKey = obj;
                    }else if(obj instanceof Map){
                        Map objMap = (Map)obj;
                        if(objMap != null && objMap.containsKey("id")){
                            primaryKey = objMap.get("id");
                        }
                    }else{
                        primaryKey = ReflectUtil.getFieldValue(obj,"id");
                    }

                    if(isNull(primaryKey)){
                        throw new BusinessException("缺少primaryKey参数");
                    }

                    String className =  proxyClass.getName().substring(proxyClass.getName().lastIndexOf(".")+1);
                    String beanName = className.substring(0,1).toLowerCase() + className.substring(1);
                    Object bean = SpringContextHolder.getBean(beanName);
                    Method mtd = proxyClass.getMethod("selectByPrimaryKey", Object.class);
                    Object objNew = mtd.invoke(bean,primaryKey);
                    cache(annotationType,primaryKey,objNew,source,RabbitMqOperationTypeEnum.OPR_UPDATE.name());
                }else if("updateByPrimaryKeySelective".equals(methodName)){
                    Object parameterObj = args[0];
                    if(parameterObj != null){
                        Object primaryKey = ReflectUtil.getFieldValue(parameterObj,"id");
                        if(isNull(primaryKey)){
                            throw new BusinessException("缺少primaryKey参数");
                        }

                        String className =  proxyClass.getName().substring(proxyClass.getName().lastIndexOf(".")+1);
                        String beanName = className.substring(0,1).toLowerCase() + className.substring(1);
                        Object bean = SpringContextHolder.getBean(beanName);
                        Method mtd = proxyClass.getMethod("selectByPrimaryKey", Object.class);
                        Object objNew = mtd.invoke(bean,primaryKey);
                        cache(annotationType,primaryKey,objNew,source,RabbitMqOperationTypeEnum.OPR_UPDATE.name());
                    }else{
                        throw new BusinessException("缺少实体参数");
                    }
                }else if("updateByExample".equals(methodName)){
                    Object parameterObj = args[1];
                    if(parameterObj != null){
                        Example example = (Example)parameterObj;

                        String className =  proxyClass.getName().substring(proxyClass.getName().lastIndexOf(".")+1);
                        String beanName = className.substring(0,1).toLowerCase() + className.substring(1);
                        Object bean = SpringContextHolder.getBean(beanName);
                        Method mtd = proxyClass.getMethod("selectByExample", Object.class);
                        Object objList = mtd.invoke(bean,example);
                        if(objList != null){
                            List list = (List)objList;
                            for (Object o : list) {
                                Object primaryKey = ReflectUtil.getFieldValue(o,"id");
                                cache(annotationType,primaryKey,o,source,RabbitMqOperationTypeEnum.OPR_UPDATE.name());
                            }
                        }
                    }
                }else if("deleteByPrimaryKey".equals(methodName)){
                    Object primaryKey = null;
                    Object obj = args[0];
                    if(obj instanceof Long){
                        primaryKey = obj;
                    }else if(obj instanceof Map){
                        Map objMap = (Map)obj;
                        if(objMap != null && objMap.containsKey("id")){
                            primaryKey = objMap.get("id");
                        }
                    }

                    String localCachedKey = String.format(EhcacheKeyEnum.EHCACHE_KEY_SELECT_BY_PRIMARY_KEY.getStringValue(), source,Convert.toStr(primaryKey));
                    ehcacheTools.evictIfPresent(EHCACHE_KEY_POCACHE,localCachedKey);
                    ehcacheTools.evictIfPresent(EHCACHE_KEY_POCACHEIFABSENT,localCachedKey);

                    String redisKey = String.format(RedisKeyEnum.REDIS_KEY_PRIMARY_SELECT_BY_PRIMARY_KEY.getStringValue(), source,Convert.toStr(primaryKey));
                    springRedisTools.deleteByKey(redisKey);


                    /**
                     * 通知其他复杂缓存
                     */
                    MqSenderUtil.cacheSender(source,primaryKey,RabbitMqOperationTypeEnum.OPR_DEL.name());
                }else if("deleteByExample".equals(methodName)){
                    Object parameterObj = args[0];
                    if(parameterObj != null){
                        Example example = (Example)parameterObj;

                        String className =  proxyClass.getName().substring(proxyClass.getName().lastIndexOf(".")+1);
                        String beanName = className.substring(0,1).toLowerCase() + className.substring(1);
                        Object bean = SpringContextHolder.getBean(beanName);
                        Method mtd = proxyClass.getMethod("selectByExample", Object.class);
                        Object objList = mtd.invoke(bean,example);
                        if(objList != null) {
                            List list = (List) objList;
                            for (Object o : list) {
                                Object primaryKey = ReflectUtil.getFieldValue(o,"id");

                                String localCachedKey = String.format(EhcacheKeyEnum.EHCACHE_KEY_SELECT_BY_PRIMARY_KEY.getStringValue(), source,Convert.toStr(primaryKey));
                                ehcacheTools.evictIfPresent(EHCACHE_KEY_POCACHE,localCachedKey);
                                ehcacheTools.evictIfPresent(EHCACHE_KEY_POCACHEIFABSENT,localCachedKey);

                                String redisKey = String.format(RedisKeyEnum.REDIS_KEY_PRIMARY_SELECT_BY_PRIMARY_KEY.getStringValue(),source, Convert.toStr(primaryKey));
                                springRedisTools.deleteByKey(redisKey);

                                /**
                                 * 通知其他复杂缓存处理
                                 */
                                MqSenderUtil.cacheSender(source,primaryKey,RabbitMqOperationTypeEnum.OPR_DEL.name());
                            }
                        }
                    }
                }
            }
        }
    }




    /**
     * 新增、修改，设置缓存
     * @param annotationType
     * @param primaryKey
     * @param objNew
     * @param source
     * @param opt
     */
    private void cache(int annotationType,Object primaryKey,Object objNew,String source,String opt){
        EhcacheTools ehcacheTools = SpringContextHolder.getBean("ehcacheTools");
        SpringRedisTools springRedisTools = SpringContextHolder.getBean("springRedisTools");
        if(annotationType == 1){
            String localCachedKey = String.format(EhcacheKeyEnum.EHCACHE_KEY_SELECT_BY_PRIMARY_KEY.getStringValue(),source, Convert.toStr(primaryKey));
            ehcacheTools.put(EHCACHE_KEY_POCACHE,localCachedKey,objNew);
        }else if(annotationType == 2){
            String redisKey = String.format(RedisKeyEnum.REDIS_KEY_PRIMARY_SELECT_BY_PRIMARY_KEY.getStringValue(), source,Convert.toStr(primaryKey));
            if(springRedisTools.hasKey(redisKey)){
                springRedisTools.deleteByKey(redisKey);
            }
            springRedisTools.addData(redisKey,JSONObject.toJSONString(objNew),DATA_EXP,TimeUnit.SECONDS);
        }else if(annotationType == 3){
            String localCachedKey = String.format(EhcacheKeyEnum.EHCACHE_KEY_SELECT_BY_PRIMARY_KEY.getStringValue(),source, Convert.toStr(primaryKey));
            ehcacheTools.put(EHCACHE_KEY_POCACHE,localCachedKey,objNew);

            String redisKey = String.format(RedisKeyEnum.REDIS_KEY_PRIMARY_SELECT_BY_PRIMARY_KEY.getStringValue(), source,Convert.toStr(primaryKey));
            if(springRedisTools.hasKey(redisKey)){
                springRedisTools.deleteByKey(redisKey);
            }
            springRedisTools.addData(redisKey,JSONObject.toJSONString(objNew),DATA_EXP,TimeUnit.SECONDS);
        }

        // 如果有更复杂的缓存，则通过mq去通知更新
        MqSenderUtil.cacheSender(source,objNew,opt);
    }

    private boolean isNull(Object primaryKey){
        if(primaryKey == null){
            return true;
        }

        if (primaryKey instanceof String){
            if(MyStringUtils.isNotBlank(MyStringUtils.null2String(primaryKey))){
                return false;
            }
            return true;
        }else if(primaryKey instanceof Long){
            if(primaryKey == null){
                return true;
            }else{
                if(Convert.toLong(primaryKey) == 0){
                    return true;
                }
                return false;
            }
        }else if(primaryKey instanceof Integer){
            if(primaryKey == null){
                return true;
            }else{
                if(Convert.toInt(primaryKey) == 0){
                    return true;
                }
                return false;
            }
        }else{
            throw new BusinessException("未知的主键类型");
        }
    }
}
