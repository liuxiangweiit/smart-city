/**
 * Created by wust on 2019-12-16 14:18:38
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.common.util;

import com.sc.common.exception.BusinessException;
import org.springframework.core.env.Environment;

/**
 * @author: wust
 * @date: Created in 2019-12-16 14:18:38
 * @description:
 *
 */
public class MyIdUtil {
    private MyIdUtil(){}
    public static long getId(){
        Environment environment = SpringContextHolder.getBean("environment");
        if(environment == null){
            throw new BusinessException("该bean[environment]没有注册");
        }

        if(!environment.containsProperty("workerId")){
            throw new BusinessException("请配置机房标识参数[workerId]");
        }

        if(!environment.containsProperty("datacenterId")){
            throw new BusinessException("请配置机器标识参数[datacenterId]");
        }

        long workerId = Long.parseLong(environment.getProperty("workerId"));
        long datacenterId = Long.parseLong(environment.getProperty("datacenterId"));

        long id = cn.hutool.core.util.IdUtil.getSnowflake(workerId,datacenterId).nextId();

        return id;
    }


    public static void main(String[] args) throws InterruptedException {
        for(int i=0;i<200;i++) {
            long id = cn.hutool.core.util.IdUtil.getSnowflake(1, 1).nextId();
           System.out.println(id + "=" + "ds"+i + "-master0" + "," + "ds"+i + "-master1" + "," + "ds"+i + "-slave0" + "," + "ds"+i + "-slave1");
        }
        long start = System.currentTimeMillis();

        Thread.sleep(1000);

        long end = System.currentTimeMillis();
        System.out.println((end - start)/1000);
    }
}
