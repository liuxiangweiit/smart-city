package com.sc.common.entity.admin.user;

/**
 * Created by wust on 2019/4/18.
 */
public class SysUserList extends SysUser {
    private static final long serialVersionUID = 382039842212175851L;

    private String sexLabel;
    private String statusLabel;
    private String typeLabel;
    private String onlineStatusLabel;
    private String agentName;
    private String parentCompanyName;
    private String branchCompanyName;

    public String getSexLabel() {
        return sexLabel;
    }

    public void setSexLabel(String sexLabel) {
        this.sexLabel = sexLabel;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }

    public String getOnlineStatusLabel() {
        return onlineStatusLabel;
    }

    public void setOnlineStatusLabel(String onlineStatusLabel) {
        this.onlineStatusLabel = onlineStatusLabel;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getParentCompanyName() {
        return parentCompanyName;
    }

    public void setParentCompanyName(String parentCompanyName) {
        this.parentCompanyName = parentCompanyName;
    }

    public String getBranchCompanyName() {
        return branchCompanyName;
    }

    public void setBranchCompanyName(String branchCompanyName) {
        this.branchCompanyName = branchCompanyName;
    }
}
