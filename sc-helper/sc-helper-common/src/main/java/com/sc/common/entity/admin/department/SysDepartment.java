package com.sc.common.entity.admin.department;

import com.sc.common.annotations.EnableDistributedCaching;
import com.sc.common.annotations.EnableLocalCaching;
import com.sc.common.entity.BaseEntity;


/**
 * Created by wust on 2019/6/3.
 */
@EnableLocalCaching
@EnableDistributedCaching
public class SysDepartment extends BaseEntity {
    private static final long serialVersionUID = -2583497779223895688L;

    private String code;
    private String name;	        // 名称
    private String leader;		    // 公司负责人
    private String description;     // 描述
    private String type;
    private Long agentId;
    private Long parentCompanyId;
    private Long branchCompanyId;
    private Long projectId;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getParentCompanyId() {
        return parentCompanyId;
    }

    public void setParentCompanyId(Long parentCompanyId) {
        this.parentCompanyId = parentCompanyId;
    }

    public Long getBranchCompanyId() {
        return branchCompanyId;
    }

    public void setBranchCompanyId(Long branchCompanyId) {
        this.branchCompanyId = branchCompanyId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return super.toString() + "\nSysDepartment{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", leader='" + leader + '\'' +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", agentId=" + agentId +
                ", parentCompanyId=" + parentCompanyId +
                ", branchCompanyId=" + branchCompanyId +
                ", projectId=" + projectId +
                '}';
    }
}
