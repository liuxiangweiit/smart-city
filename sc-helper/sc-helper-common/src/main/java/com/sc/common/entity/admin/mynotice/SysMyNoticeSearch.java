/**
 * Created by wust on 2019-10-21 14:26:33
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.common.entity.admin.mynotice;


import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: Created in 2019-10-21 14:26:33
 * @description:
 *
 */
public class SysMyNoticeSearch extends SysMyNotice {
    private static final long serialVersionUID = 7451504657824029163L;

    private String title;

    private PageDto pageDto;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}
