package com.sc.common.util.cache;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import com.alibaba.fastjson.JSONObject;
import com.sc.common.entity.admin.apptoken.SysAppToken;
import com.sc.common.entity.admin.datasource.SysDataSource;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.entity.admin.datasource.detail.SysDataSourceDetail;
import com.sc.common.entity.admin.lookup.SysLookup;
import com.sc.common.entity.admin.lookup.SysLookupComparator;
import com.sc.common.entity.admin.project.SysProject;
import com.sc.common.enums.EhcacheKeyEnum;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.exception.BusinessException;
import com.sc.common.util.SpringContextHolder;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Function: 复杂缓存数据获取工具
 * Reason: 可以从本地缓存或者分布式缓存获取数据
 * Date:2018/7/4
 *
 * @author wust
 */
@Component
public class DataDictionaryUtil {

    private static SpringRedisTools springRedisTools = null;

    private DataDictionaryUtil(){
    }

    public static void setSpringRedisTools(SpringRedisTools springRedisTools){
        DataDictionaryUtil.springRedisTools = springRedisTools;
    }


    /**
     * 根据primaryKey获取分布式缓存持久化对象
     * @param primaryKey
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T  getApplicationPOByPrimaryKey(Object primaryKey, Class<? extends T> clazz){
        SpringRedisTools springRedisTools = SpringContextHolder.getBean("springRedisTools");
        if(springRedisTools == null){
            throw new BusinessException("Spring容器没有该bean[springRedisTools]");
        }

        String source = clazz.getSimpleName();
        String redisKey = String.format(RedisKeyEnum.REDIS_KEY_PRIMARY_SELECT_BY_PRIMARY_KEY.getStringValue(), source, Convert.toStr(primaryKey));
        Object obj = springRedisTools.getByKey(redisKey);
        if(obj != null){
            T t = JSONObject.parseObject(obj.toString(),clazz);
            return t;
        }
        return null;
    }


    /**
     * 根据primaryKey获取本地缓存持久化对象
     * @param primaryKey
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T  getLocalPOByPrimaryKey(Object primaryKey, Class<? extends T> clazz){
        EhcacheTools ehcacheTools = SpringContextHolder.getBean("ehcacheTools");
        if(ehcacheTools == null){
            throw new BusinessException("Spring容器没有该bean[ehcacheTools]");
        }

        String source = clazz.getSimpleName();
        String key = String.format(EhcacheKeyEnum.EHCACHE_KEY_SELECT_BY_PRIMARY_KEY.getStringValue(),source, Convert.toStr(primaryKey));
        T t = ehcacheTools.get("poCache",key,clazz);
        if(t != null){
            return t;
        }
        return null;
    }



    /**************************************Lookup S****************************************/

    /**
     * 根据parentCode获取子列表
     * @param parentCode
     * @return
     */
    public static List<SysLookup> getLookupListByParentCode(String lan, String parentCode) {
        String key = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_LOOKUP_BY_PID_AND_LANG.getStringValue(),parentCode,lan);
        Map map = getRedisSpringBean().getMap(key);
        if(CollectionUtil.isNotEmpty(map)){
            List<SysLookup> lookupList = new ArrayList<>(map.size());
            Set keys = map.keySet();
            for (Object o : keys) {
                SysLookup sysLookup = map.get(o.toString()) == null ? null : (SysLookup)map.get(o.toString());
                lookupList.add(sysLookup);
            }

            SysLookupComparator lookupComparator = new SysLookupComparator();
            lookupList.sort(lookupComparator);
            return lookupList;
        }
        return null;
    }

    /**
     * 根据code获取name
     * @param codeValue
     * @return
     */
    public static String getLookupNameByCode(String lan,String codeValue) {
        String key = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_LOOKUP_BY_CODE_AND_LANG.getStringValue(),codeValue,lan);
        Map map = getRedisSpringBean().getMap(key);
        if(CollectionUtil.isNotEmpty(map)){
            SysLookup sysLookup = BeanUtil.mapToBean(map,SysLookup.class,true);
            String name = sysLookup.getName();
            return name;
        }
        return null;
    }

    /**
     * 根据rootCode和name获取code
     * @param rootCode
     * @param name
     * @return
     */
    public static String getLookupCodeByRootCodeAndName(String lan,String rootCode, String name) {
        String key = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_LOOKUP_BY_ROOT_CODE_AND_NAME_AND_LANG.getStringValue(),rootCode,name,lan);
        Map map = getRedisSpringBean().getMap(key);
        if(CollectionUtil.isNotEmpty(map)){
            SysLookup sysLookup = BeanUtil.mapToBean(map,SysLookup.class,true);
            return sysLookup.getCode();
        }
        return null;
    }
    /**************************************Lookup E****************************************/




    /**
     * 根据当前服务和项目Id获取数据源
     * @param dataSourceGroupName
     * @param projectId
     * @return
     */
    public static SysDataSource getDataSourceByProjectId(String dataSourceGroupName, Long projectId){
        String key = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_DATA_SOURCE_BY_GROUP_NAME_AND_PROJECT_ID.getStringValue(),dataSourceGroupName,projectId);
        Map map = getRedisSpringBean().getMap(key);
        if(CollectionUtil.isNotEmpty(map)){
            SysDataSource dataSource = BeanUtil.mapToBean(map,SysDataSource.class,true);
            return dataSource;
        }
        return null;
    }

    /**
     * 根据数据源分组名称获取数据源组
     * @param dataSourceGroupName
     * @return
     */
    public static Map<Long,SysDataSource> getDataSourceByGroupName(String dataSourceGroupName){
        String key = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_DATA_SOURCE_BY_GROUP_NAME.getStringValue(),dataSourceGroupName);
        Map map = getRedisSpringBean().getMap(key);
        if(CollectionUtil.isNotEmpty(map)){
            return map;
        }
        return null;
    }


    public static Map<Long, SysDataSourceDetail> getDataSourceDetailsByDataSourceId(Long dataSourceId){
        String key = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_DATA_SOURCE_DETAIL_BY_DATA_SOURCE_ID.getStringValue(),dataSourceId);
        Map map = getRedisSpringBean().getMap(key);
        if(CollectionUtil.isNotEmpty(map)){
            return map;
        }
        return null;
    }

    public static SysAppToken getAppTokenByAppId(String appId){
        String key = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_APP_TOKEN_BY_APP_ID.getStringValue(),appId);
        Map map = getRedisSpringBean().getMap(key);
        if(CollectionUtil.isNotEmpty(map)){
            SysAppToken result = BeanUtil.mapToBean(map,SysAppToken.class,true);
            return result;
        }
        return null;
    }




    public static SysProject getProjectByName(String name){
        String key = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_PROJECT_BY_NAME.getStringValue(),name);
        Map map = getRedisSpringBean().getMap(key);
        if(CollectionUtil.isNotEmpty(map)){
            SysProject result = BeanUtil.mapToBean(map,SysProject.class,true);
            return result;
        }
        return null;
    }




    public static SysAccount getAccountByCode(String code){
        String key = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_ACCOUNT_BY_CODE.getStringValue(),code);
        Map map = getRedisSpringBean().getMap(key);
        if(CollectionUtil.isNotEmpty(map)){
            SysAccount result = BeanUtil.mapToBean(map,SysAccount.class,true);
            return result;
        }
        return null;
    }


    public static Map getGatewayByMac(String mac){
        String key = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_GATEWAY_BY_MAC.getStringValue(),mac);
        Map map = getRedisSpringBean().getMap(key);
        if(CollectionUtil.isNotEmpty(map)){
            return map;
        }
        return null;
    }


    private static SpringRedisTools getRedisSpringBean(){
        if(springRedisTools == null){
            springRedisTools = SpringContextHolder.getBean("springRedisTools");
        }
        return springRedisTools;
    }
}
