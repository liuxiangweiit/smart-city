package com.sc.common.service;

/**
 * 自定义初始化接口：如果想要自定义各个接口的初始化顺序，则可以实现该接口，并加上@Order(整数)注解来实现初始化顺序
 * 如果没有顺序要求，则可以在任何类使用 @PostConstruct即可实现初始化功能，建议还是实现此接口来做相关初始化工作，在维护工作上也方便查找。
 */
public interface InitializtionService {
    void init();
}
