package com.sc.easyexcel.xmlobject.common;


import cn.hutool.core.util.StrUtil;

/**
 * SQL元素
 */
public class XMLSQL4Export{
    // attributeId属性
    private String attributeId;

    // SQL文本
    private String sqlText;

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public String getSqlText() {
        return sqlText;
    }

    public void setSqlText(String sqlText) {
        this.sqlText = sqlText;
    }

    public boolean isEmpty(){
        if(StrUtil.isEmpty(attributeId)
                && StrUtil.isEmpty(sqlText)){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "XMLSQL4Export{" +
                "attributeId='" + attributeId + '\'' +
                ", sqlText='" + sqlText + '\'' +
                '}';
    }
}