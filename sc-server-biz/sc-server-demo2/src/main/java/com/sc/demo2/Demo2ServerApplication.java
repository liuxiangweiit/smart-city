package com.sc.demo2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;

@EnableSwagger2
@EnableEurekaClient
@EnableTransactionManagement
@MapperScan(basePackages = {"com.sc.common.dao","com.sc.demo2.core.dao"})
@SpringBootApplication(scanBasePackages = {"com.sc"})
public class Demo2ServerApplication {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Demo2ServerApplication.class);
        app.setWebApplicationType(WebApplicationType.REACTIVE);
        SpringApplication.run(Demo2ServerApplication.class, args);
    }
}
