package com.sc.workorder.core.service.impl;

import com.sc.workorder.core.dao.WoWorkOrderUserMapper;
import com.sc.workorder.core.service.WoWorkOrderUserService;
import com.sc.workorder.entity.WoWorkOrderUser;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.sc.common.dto.WebResponseDto;

/**
 * @author: wust
 * @date: 2020-04-01 10:18:23
 * @description:
 */
@Service("woWorkOrderUserServiceImpl")
public class WoWorkOrderUserServiceImpl extends BaseServiceImpl<WoWorkOrderUser> implements WoWorkOrderUserService{
    @Autowired
    private WoWorkOrderUserMapper woWorkOrderUserMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return woWorkOrderUserMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
