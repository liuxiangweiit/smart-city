package com.sc.admin.api;


import com.sc.admin.api.fallback.DistributedFileServiceFallback;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.distributedfile.SysDistributedFile;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "admin-server",fallback = DistributedFileServiceFallback.class)
public interface DistributedFileService {
    String API_PREFIX = "/api/feign/v1/DistributedFileFeignApi";
    String INSERT = API_PREFIX + "/insert";
    String DELETE_BY_PRIMARY_KEY = API_PREFIX + "/deleteByPrimaryKey";
    String SELECT_ONE = API_PREFIX + "/selectOne";

    @RequestMapping(value = INSERT,method= RequestMethod.POST, consumes = "application/json")
    WebResponseDto insert(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody SysDistributedFile entity);

    @RequestMapping(value = DELETE_BY_PRIMARY_KEY,method= RequestMethod.POST, consumes = "application/json")
    WebResponseDto deleteByPrimaryKey(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestParam Long id);

    @RequestMapping(value = SELECT_ONE,method= RequestMethod.POST, consumes = "application/json")
    WebResponseDto selectOne(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody SysDistributedFile search);
}
