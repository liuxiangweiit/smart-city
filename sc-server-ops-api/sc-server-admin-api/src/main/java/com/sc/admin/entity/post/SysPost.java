package com.sc.admin.entity.post;

import com.sc.common.entity.BaseEntity;
import lombok.*;
import io.swagger.annotations.ApiModel;

/**
 * @author: wust
 * @date: 2020-12-03 11:14:47
 * @description:
 */
 @ApiModel(description = "实体对象-SysPost")
 @NoArgsConstructor
 @AllArgsConstructor
 @ToString
 @Data
public class SysPost extends BaseEntity {
        private String code;
    private String name;
    private String description;
                            
}