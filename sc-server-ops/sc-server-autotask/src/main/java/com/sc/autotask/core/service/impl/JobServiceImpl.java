package com.sc.autotask.core.service.impl;

import com.sc.autotask.core.dao.JobMapper;
import com.sc.autotask.core.service.JobService;
import com.sc.autotask.entity.jobandtrigger.QrtzJobAndTriggerList;
import com.sc.autotask.entity.jobandtrigger.QrtzJobAndTriggerSearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Created by wust on 2019/6/13.
 */
@Service("jobServiceImpl")
public class JobServiceImpl implements JobService {
    @Autowired
    private JobMapper jobMapper;

    @Override
    public List<QrtzJobAndTriggerList> selectByPageNumSize(QrtzJobAndTriggerSearch search, int pageNum, int pageSize) {
        return jobMapper.selectByPageNumSize(search,pageNum,pageSize);
    }
}
