package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.notification.SysNotificationTimeline;

/**
 * @author: wust
 * @date: 2020-04-09 13:26:08
 * @description:
 */
public interface SysNotificationTimelineMapper extends IBaseMapper<SysNotificationTimeline>{
}