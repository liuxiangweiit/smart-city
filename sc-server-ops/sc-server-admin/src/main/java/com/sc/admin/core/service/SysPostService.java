package com.sc.admin.core.service;

import com.sc.common.service.BaseService;
import com.sc.admin.entity.post.SysPost;

/**
 * @author: wust
 * @date: 2020-12-03 11:14:47
 * @description:
 */
public interface SysPostService extends BaseService<SysPost>{
}
