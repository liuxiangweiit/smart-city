/**
 * Created by wust on 2020-03-30 10:44:32
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.cache;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.dao.SysCompanyMapper;
import com.sc.admin.core.dao.SysProjectMapper;
import com.sc.admin.core.dao.SysUserMapper;
import com.sc.admin.core.dao.SysUserOrganizationMapper;
import com.sc.common.annotations.EnableComplexCaching;
import com.sc.common.entity.admin.company.SysCompany;
import com.sc.common.entity.admin.project.SysProject;
import com.sc.common.entity.admin.user.SysUser;
import com.sc.common.entity.admin.userorganization.SysUserOrganization;
import com.sc.common.enums.DataDictionaryEnum;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.cache.CacheAbstract;
import com.sc.common.util.cache.SpringRedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.*;

/**
 * @author: wust
 * @date: Created in 2020-03-30 10:44:32
 * @description: 员工组织关系缓存
 *
 */
@EnableComplexCaching(dependsOnPOSimpleName = "SysUserOrganization")
@Component
public class RedisCacheUserOrganizationBo extends CacheAbstract {
    @Autowired
    private SpringRedisTools springRedisTools;

    @Autowired
    private SysUserOrganizationMapper sysUserOrganizationMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysProjectMapper sysProjectMapper;

    @Autowired
    private SysCompanyMapper sysCompanyMapper;

    @Override
    public void init() {
        Set<String> keys1 = springRedisTools.keys(RedisKeyEnum.REDIS_KEY_HASH_GROUP_BRANCH_COMPANY_BY_USER_ID.getStringValue().replaceAll("%s_","*"));
        if(keys1 != null && keys1.size() > 0){
            springRedisTools.deleteByKey(keys1);
        }


        Set<String> keys2 = springRedisTools.keys(RedisKeyEnum.REDIS_KEY_HASH_GROUP_PROJECT_BY_USER_ID.getStringValue().replaceAll("%s_","*"));
        if(keys2 != null && keys2.size() > 0){
            springRedisTools.deleteByKey(keys2);
        }

        List<SysUser> userList = sysUserMapper.selectAll();
        if(CollectionUtil.isEmpty(userList)){
            return;
        }

        for (SysUser sysUser : userList) {
            List<Map> branchCompanyList = new ArrayList<>();
            List<Map> projectList = new ArrayList<>();
            if(DataDictionaryEnum.USER_TYPE_AGENT.getStringValue().equals(sysUser.getType())){ // 代理商账号
                /**
                 * 缓存用户-公司关系
                 * 缓存用户-项目关系
                 * 1.先找出该用户所属的代理商ID
                 * 2.然后根据代理商ID找出该代理商拥有的分公司、项目集合，这些集合就是该账号拥有的集合
                 */
                SysUserOrganization userOrganizationSearch = new SysUserOrganization();
                userOrganizationSearch.setUserId(sysUser.getId());
                List<SysUserOrganization> userOrganizations = sysUserOrganizationMapper.select(userOrganizationSearch);
                if(CollectionUtil.isNotEmpty(userOrganizations)){
                    Long agentId = userOrganizations.get(0).getAgentId();
                    branchCompanyList = sysUserOrganizationMapper.findBranchCompanyByAgentId(agentId);
                    projectList = sysUserOrganizationMapper.findProjectByAgentId(agentId);
                }
            }else if(DataDictionaryEnum.USER_TYPE_PARENT_COMPANY.getStringValue().equals(sysUser.getType())){ // 总公司账号
                /**
                 * 缓存用户-公司关系
                 * 缓存用户-项目关系
                 * 1.先找出该用户所属的总公司ID
                 * 2.然后根据总公司ID找出其拥有的分公司、项目集合，这些集合就是该账号拥有的集合
                 */
                SysUserOrganization userOrganizationSearch = new SysUserOrganization();
                userOrganizationSearch.setUserId(sysUser.getId());
                List<SysUserOrganization> userOrganizations = sysUserOrganizationMapper.select(userOrganizationSearch);
                if(CollectionUtil.isNotEmpty(userOrganizations)){
                    Long parentCompanyId = userOrganizations.get(0).getParentCompanyId();
                    branchCompanyList = sysUserOrganizationMapper.findBranchCompanyByParentCompanyId(parentCompanyId);
                    projectList = sysUserOrganizationMapper.findProjectByParentCompanyId(parentCompanyId);
                }
            }else if(DataDictionaryEnum.USER_TYPE_BRANCH_COMPANY.getStringValue().equals(sysUser.getType())){ // 分公司账号
                /**
                 * 缓存用户-公司关系
                 * 缓存用户-项目关系
                 * 1.可直接查询出该账号对应的分公司
                 * 2.可直接查询出该账号对应的项目集合
                 */
                SysUserOrganization userOrganizationSearch = new SysUserOrganization();
                userOrganizationSearch.setUserId(sysUser.getId());
                List<SysUserOrganization> userOrganizations = sysUserOrganizationMapper.select(userOrganizationSearch);
                if(CollectionUtil.isNotEmpty(userOrganizations)){
                    Long branchCompanyId = userOrganizations.get(0).getBranchCompanyId();
                    branchCompanyList = sysUserOrganizationMapper.findBranchCompanyByBranchCompanyId(branchCompanyId);
                    projectList = sysUserOrganizationMapper.findProjectByBranchCompanyId(branchCompanyId);
                }
            }else if(DataDictionaryEnum.USER_TYPE_PARENT_COMPANY.getStringValue().equals(sysUser.getType())){ // 项目账号
                /**
                 * 缓存用户-公司关系
                 * 缓存用户-项目关系
                 * 1.可直接查询出该账号对应的分公司
                 * 2.可直接查询出该账号对应的项目集合
                 */
                SysUserOrganization userOrganizationSearch = new SysUserOrganization();
                userOrganizationSearch.setUserId(sysUser.getId());
                List<SysUserOrganization> userOrganizations = sysUserOrganizationMapper.select(userOrganizationSearch);
                if(CollectionUtil.isNotEmpty(userOrganizations)){
                    Long projectId = userOrganizations.get(0).getProjectId();
                    branchCompanyList = sysUserOrganizationMapper.findBranchCompanyByProjectId(projectId);
                    projectList = sysUserOrganizationMapper.findProjectByProjectId(projectId);
                }
            }else if(DataDictionaryEnum.USER_TYPE_BUSINESS.getStringValue().equals(sysUser.getType())){ // 业务员账号
                branchCompanyList = sysUserOrganizationMapper.findBranchCompanyByUserId(sysUser.getId());
                projectList = sysUserOrganizationMapper.findProjectByUserId(sysUser.getId());
            }




            if(CollectionUtil.isNotEmpty(branchCompanyList)){
                String key1 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_BRANCH_COMPANY_BY_USER_ID.getStringValue(),sysUser.getId());
                Map mapValue1 = new HashMap();
                for (Map map : branchCompanyList) {
                    if(CollectionUtil.isEmpty(map)){
                        continue;
                    }
                    mapValue1.put(map.get("id").toString(),map);
                }
                springRedisTools.addMap(key1,mapValue1);
            }


            if(CollectionUtil.isNotEmpty(projectList)){
                String key2 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_PROJECT_BY_USER_ID.getStringValue(),sysUser.getId());
                Map mapValue2 = new HashMap();
                for (Map map : projectList) {
                    if(CollectionUtil.isEmpty(map)){
                        continue;
                    }
                    mapValue2.put(map.get("id").toString(),map);
                }
                springRedisTools.addMap(key2,mapValue2);
            }
        }
    }

    @Override
    public void reset() {

    }

    @Override
    public void add(Object obj){
        if(obj == null){
            return;
        }

        SysUserOrganization entity = null;

        if(obj instanceof Long){
            entity = sysUserOrganizationMapper.selectByPrimaryKey(obj);
        }else if(obj instanceof SysUserOrganization){
            entity = (SysUserOrganization)obj;
        }else if(obj instanceof Map){
            entity = JSONObject.parseObject(JSONObject.toJSONString(obj),SysUserOrganization.class);
        }

        if(entity == null){
            return;
        }

        Long projectId = entity.getProjectId();
        Long branchCompanyId = entity.getBranchCompanyId();
        Long userId = entity.getUserId();

        cacheProjectByUserId(projectId,userId);
        cacheBranchCompanyByUserId(branchCompanyId,userId);
    }

    @Override
    public void batchAdd(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                add(o);
            }
        }
    }

    @Override
    public void updateByPrimaryKey(Object primaryKey){

    }

    @Override
    public void batchUpdate(List<Object> list){

    }

    @Override
    public void deleteByPrimaryKey(Object primaryKey){
        String key1 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_BRANCH_COMPANY_BY_USER_ID.getStringValue(),primaryKey);
        springRedisTools.deleteByKey(key1);
    }

    @Override
    public void batchDelete(List<Object> primaryKeys){
        if(CollectionUtil.isNotEmpty(primaryKeys)){
            for (Object primaryKey : primaryKeys) {
                deleteByPrimaryKey(primaryKey);
            }
        }
    }


    /**
     * 根据userId缓存项目
     * @param projectId
     * @param userId
     */
    private void cacheProjectByUserId(Long projectId,Long userId){
        if(projectId != null){
            SysProject project = sysProjectMapper.selectByPrimaryKey(projectId);
            if(project == null || project.getId() == null){
                return;
            }
            String key1 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_PROJECT_BY_USER_ID.getStringValue(),userId);
            if(springRedisTools.hasKey(key1)){
                Map map = springRedisTools.getMap(key1);
                if (map == null) {
                    map = new HashMap();
                }
                map.put(project.getId().toString(), BeanUtil.beanToMap(project));
                springRedisTools.addMap(key1,map);
            }else{
                Map map = new HashMap();
                map.put(project.getId().toString(), BeanUtil.beanToMap(project));
                springRedisTools.addMap(key1,map);
            }
        }
    }

    /**
     * 根据userId缓存分公司
     * @param branchCompanyId
     * @param userId
     */
    private void cacheBranchCompanyByUserId(Long branchCompanyId,Long userId){
        if(branchCompanyId != null){
            SysCompany branchCompany = sysCompanyMapper.selectByPrimaryKey(branchCompanyId);
            if(branchCompany == null || branchCompany.getId() == null){
                return;
            }

            String key1 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_BRANCH_COMPANY_BY_USER_ID.getStringValue(),userId);
            if(springRedisTools.hasKey(key1)){
                Map map = springRedisTools.getMap(key1);
                if (map == null) {
                    map = new HashMap();
                }
                map.put(branchCompany.getId().toString(), BeanUtil.beanToMap(branchCompany));
                springRedisTools.addMap(key1,map);
            }else{
                Map map = new HashMap();
                map.put(branchCompany.getId().toString(), BeanUtil.beanToMap(branchCompany));
                springRedisTools.addMap(key1,map);
            }
        }
    }
}
