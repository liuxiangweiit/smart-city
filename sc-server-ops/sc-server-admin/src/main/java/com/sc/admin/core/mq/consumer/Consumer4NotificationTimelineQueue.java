/**
 * Created by wust on 2019-11-09 14:00:06
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.mq.consumer;


import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.service.SysNotificationTimelineService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: wust
 * @date: Created in 2020-02-27 14:00:06
 * @description: 报警记录消费队列
 */
@RabbitListener(
        containerFactory = "singleListenerContainer",
        bindings = @QueueBinding(
                value = @Queue(
                        value = "${spring.rabbitmq.notification-timeline.queue.name}",
                        durable = "${spring.rabbitmq.notification-timeline.queue.durable}"
                ),
                exchange = @Exchange(
                        value = "${spring.rabbitmq.notification-timeline.exchange.name}",
                        durable = "${spring.rabbitmq.notification-timeline.exchange.durable}",
                        type = "${spring.rabbitmq.notification-timeline.exchange.type}",
                        ignoreDeclarationExceptions = "${spring.rabbitmq.notification-timeline.exchange.ignoreDeclarationExceptions}"
                ),
                key = "${spring.rabbitmq.notification-timeline.routing-key}"
        )
)
@Component
public class Consumer4NotificationTimelineQueue {
    static Logger logger = LogManager.getLogger(Consumer4NotificationTimelineQueue.class);

    @Autowired
    private SysNotificationTimelineService sysNotificationTimelineServiceImpl;


    @RabbitHandler
    public void process(JSONObject jsonObject) {
        logger.info("报警记录费队列,{}", jsonObject);
        sysNotificationTimelineServiceImpl.createByMq(jsonObject);
    }
}
