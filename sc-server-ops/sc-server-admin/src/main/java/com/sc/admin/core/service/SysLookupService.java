package com.sc.admin.core.service;


import com.sc.common.entity.admin.lookup.SysLookup;
import com.sc.common.service.BaseService;

/**
 * Created by wust on 2019/4/29.
 */
public interface SysLookupService extends BaseService<SysLookup> {
}
