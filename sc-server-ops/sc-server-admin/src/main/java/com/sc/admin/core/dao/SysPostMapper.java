package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.admin.entity.post.SysPost;

/**
 * @author: wust
 * @date: 2020-12-03 11:14:47
 * @description:
 */
public interface SysPostMapper extends IBaseMapper<SysPost>{
}