package com.sc.workorder.entity;


/**
 * @author: wust
 * @date: 2020-04-09 13:13:37
 * @description:
 */
public class WoWorkOrderTimelineList extends WoWorkOrderTimeline {
    private String typeLabel;

    public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }
}