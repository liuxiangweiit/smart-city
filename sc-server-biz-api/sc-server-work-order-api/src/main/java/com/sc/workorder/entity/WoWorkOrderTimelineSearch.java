package com.sc.workorder.entity;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2020-04-09 13:13:37
 * @description:
 */
public class WoWorkOrderTimelineSearch extends WoWorkOrderTimeline {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}