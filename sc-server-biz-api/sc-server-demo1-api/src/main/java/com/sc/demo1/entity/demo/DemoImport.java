/**
 * Created by wust on 2020-01-07 12:59:11
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.demo1.entity.demo;

/**
 * @author: wust
 * @date: Created in 2020-01-07 12:59:11
 * @description:
 *
 */
public class DemoImport extends Demo {
    private static final long serialVersionUID = 2864097245620833542L;

    // 行号，必须加
    private Integer row;
    // 是否成功，必须加
    private Boolean successFlag;
    // 错误原因，必须加
    private String errorMessage;

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Boolean getSuccessFlag() {
        return successFlag;
    }

    public void setSuccessFlag(Boolean successFlag) {
        this.successFlag = successFlag;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return super.toString() + "\nSysUserImport{" +
                "row=" + row +
                ", successFlag=" + successFlag +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
